﻿using UnityEngine;
using System.Collections;

public class LevelSelect : MonoBehaviour {

    public string easyLevel;
    public string mediumLevel;
    public string hardLevel;
    public static string LevelID;

    public void EasyLevel()
    {
        Application.LoadLevel(easyLevel);
        LevelID = "easy";
    }

    public void MediumLevel()
    {
        Application.LoadLevel(mediumLevel);
        LevelID = "medium";
    }

    public void HardLevel()
    {
        Application.LoadLevel(hardLevel);
        LevelID = "hard";
    }
}
