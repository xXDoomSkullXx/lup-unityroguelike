﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Player : MovingObject {

    public int playerID;
    public Text healthText;
    public AudioClip movementSound1;
    public AudioClip movementSound2;
    public AudioClip chopSound1;
    public AudioClip chopSound2;
    public AudioClip fruitSound1;
    public AudioClip fruitSound2;
    public AudioClip sodaSound1;
    public AudioClip sodaSound2;

    private Animator animator;
    private double playerHealth;
    private int attackPower;
    private int healthPerFruit = 5;
    private int healthPerSoda = 5;
    private int healthPerPizza = 15;
    private int healthPerBurger = 10;
    private int secondsUntilNextLevel = 1;
    private double knightDamage = .7;

    protected override void Start()
    {
        base.Start();
        animator = GetComponent<Animator>();
        if (playerID <= 0) {
            playerHealth = GameController.Instance.playerCurrentHealth;
        }
        else if (playerID >= 1)
        {
            playerHealth = GameController.Instance.knightCurrentHealth;
        }
        healthText.text = "Health: " + playerHealth;
    }

    private void OnDisable()
    {
        if (playerID <= 0) {
            GameController.Instance.playerCurrentHealth = playerHealth;
        }
        else if (playerID >= 1)
        {
            GameController.Instance.knightCurrentHealth = playerHealth;
        }
    }

	void Update () {
        

        if(!GameController.Instance.isPlayerTurn)
        {
            return;
        }
        CheckIfGameOver();

        int xAxis = 0;
        int yAxis = 0;

        xAxis = (int)Input.GetAxisRaw("Horizontal");
        yAxis = (int)Input.GetAxisRaw("Vertical");

        if (xAxis != 0)
        {
            yAxis = 0;
        }

        if(xAxis != 0 || yAxis != 0)
        {
            playerHealth--;
            healthText.text = "Health: " + playerHealth;
            SoundController.Instance.PlaySingle(movementSound1, movementSound2);
            Move<Wall>(xAxis, yAxis);
            GameController.Instance.isPlayerTurn = false;

        }

	}

    private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith)
    {
        if(objectPlayerCollidedWith.tag == "Exit")
        {
            Invoke("LoadNewLevel", secondsUntilNextLevel);
            enabled = false;
        }
        else if (objectPlayerCollidedWith.tag == "Fruit")
        {
            playerHealth += healthPerFruit;
            healthText.text = "+" + healthPerFruit + " Health\n" + "Health: " + playerHealth;
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(fruitSound1, fruitSound2);
        }
        else if (objectPlayerCollidedWith.tag == "Soda")
        {

            playerHealth += healthPerSoda;
            healthText.text = "+" + healthPerSoda + " Health\n" + "Health: " + playerHealth;
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(sodaSound1, sodaSound2);
        }
        else if (objectPlayerCollidedWith.tag == "Pizza")
        {

            playerHealth += healthPerPizza;
            healthText.text = "+" + healthPerPizza + " Health\n" + "Health: " + playerHealth;
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(fruitSound1, fruitSound2);
        }
        else if (objectPlayerCollidedWith.tag == "Burger")
        {

            playerHealth += healthPerBurger;
            healthText.text = "+" + healthPerBurger + " Health\n" + "Health: " + playerHealth;
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(fruitSound1, fruitSound2);
        }
    }

    private void LoadNewLevel()
    {
        Application.LoadLevel(Application.loadedLevel);
        


    }
    protected override void HandleCollision<T>(T component)
    {
        Wall wall = component as Wall;
        animator.SetTrigger("playerAttack");
        SoundController.Instance.PlaySingle(chopSound1, chopSound2);
        if (playerID <= 0) {
            wall.DamageWall(attackPower = 1);
        }
        else if(playerID >= 0)
        {
            wall.DamageWall(attackPower = 2);
        }

    }
    public void TakeDamage(int damageReceived)
    {
        if (playerID <= 0) {
            playerHealth -= damageReceived;
            healthText.text = "-" + damageReceived + " Health\n" + "Health: " + playerHealth;
        }
        else if(playerID >= 1)
        {
            playerHealth -= damageReceived * knightDamage;
            healthText.text = "-" + damageReceived * knightDamage + " Health\n" + "Health: " + playerHealth;
        }
        animator.SetTrigger("playerHurt");
    }

    private void CheckIfGameOver()
    {
        if (playerHealth <= 0)
        {
            GameController.Instance.GameOver();
        }
    }
    
}
