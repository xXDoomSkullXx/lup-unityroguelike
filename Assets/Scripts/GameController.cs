﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public static GameController Instance;
    public bool isPlayerTurn;
    public bool areEnemiesMoving;
    public double playerCurrentHealth = 50;
    public double knightCurrentHealth = 30;
    public AudioClip gameOversound;
    public Player player;

    private BoardController boardController;
    private List<Enemy> enemies;
    private GameObject levelImage;
    private GameObject Button;
    private Text levelText;
    private bool settingUpGame;
    private int secondsUntilLevelStart = 2;
    public static int currentLevel = 1;
   

    void Awake() {


            if (Instance != null && Instance != this)
            {
                DestroyImmediate(gameObject);
                return;
            }

            Instance = this;
            DontDestroyOnLoad(gameObject);
            boardController = GetComponent<BoardController>();
            enemies = new List<Enemy>();
        }
    
    void Start()
    {
        Application.LoadLevel(Application.loadedLevel);
        currentLevel = 0;
        InitializeGame();
        
    }



    public void InitializeGame()
    {

        settingUpGame = true;
        Button = GameObject.Find("Button");
        Button.SetActive(false);
        levelImage = GameObject.Find("Level Image");
        levelText = GameObject.Find("Level Text").GetComponent<Text>();
        levelText.text = "Day " + currentLevel;
        levelImage.SetActive(true);
        enemies.Clear();
        boardController.SetupLevel(currentLevel);
        Invoke("DisableLevelImage", secondsUntilLevelStart);
    }

    private void DisableLevelImage()
    {
        levelImage.SetActive(false);
        settingUpGame = false;
        isPlayerTurn = true;
        areEnemiesMoving = false;
    }
	
    private void OnLevelWasLoaded(int levelLoaded)
    {
        currentLevel++;
        InitializeGame();
    }

	void Update () {
        if(isPlayerTurn || areEnemiesMoving || settingUpGame)
        {
            return;
        }

        StartCoroutine(MoveEnemies());
	}

    private IEnumerator MoveEnemies()
    {
        areEnemiesMoving = true;

        yield return new WaitForSeconds(0.2f);

        foreach(Enemy enemy in enemies)
        {
            enemy.MoveEnemy();
            yield return new WaitForSeconds(enemy.moveTime);
        }

        areEnemiesMoving = false;
        isPlayerTurn = true;
    }
    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }

    public void GameOver()
    {
        
        isPlayerTurn = false;
        SoundController.Instance.music.Stop();
        SoundController.Instance.PlaySingle(gameOversound);
        if (currentLevel > 1) {
            levelText.text = "You starved after " + currentLevel + " days...";
        }
        else if (currentLevel == 1)
        {
            levelText.text = "You starved after 1 day...";
        }
        PlayerPrefs.SetInt("currentLevel", currentLevel);
        levelImage.SetActive(true);
        enabled = false;
        Button.SetActive(true);
    }
}
