﻿using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    static int highscore;
    public static int currentLevel;
    Text text;

    // Set highscoreText up in the inspector.
    public Text highscoreText;

    void Awake()
    {
        text = GetComponent<Text>();
        currentLevel = PlayerPrefs.GetInt("currentLevel");
        highscore = PlayerPrefs.GetInt("highscore");
    }

    void Update()
    {
        if (currentLevel > highscore)
        {
            highscore = currentLevel;
            PlayerPrefs.SetInt("highscore", highscore);
        }
        highscoreText.text = "Highscore: " + highscore;
        text.text = "Score: " + currentLevel;
    }
}